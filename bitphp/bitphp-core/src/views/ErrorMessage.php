	<meta charset="utf8">
	<style type="text/css">
		html, body {
			padding: 0;
			margin: 0;
		}
		.bitphp-error {
			position: absolute;
			display: table;
			height: 100%;
			width: 100%;
			margin: 0;
			padding: 0;
			background-color: #f2f2f2;
			font-family: sans-serif;
			top: 0;
		}
		.bitphp-error .cell {
			display: table-cell;
		}
		.bitphp-error .vertical-middle {
			vertical-align: middle;
		}
		.bitphp-error .panel {
			color: #333;
			background-color: #fff;
			padding: 15px 20px;
			max-width: 500px;
			margin: 10px;
			border-bottom: solid 3px #fff;
		}

		.bitphp-error .panel p {
			text-align: left;
		}
		
		.bitphp-error .panel:hover {
			cursor: pointer;
			border-bottom-color: rgb(103, 136, 219);
		}

		.bitphp-error .panel:hover .highlight {
			color: rgb(103, 136, 219);
		}

		.bitphp-error .hide {
			opacity: 0;
		}

		.bitphp-error .panel:hover .hide {
			opacity: 0.8;
		}

		.bitphp-error .comand {
			padding: 10px;
			border-left: 3px solid rgb(143, 242, 143);
			font-style: italic;
		}
		
		.bitphp-error .blue {
			color: rgb(103, 136, 219);
		}

		* {
  			-webkit-transition: all 0.25s ease;
  			-moz-transition: all 0.25s ease;
  			-ms-transition: all 0.25s ease;
  			-o-transition: all 0.25s ease;
  			transition: all 0.25s ease;
		}
	</style>
	<div class="bitphp-error" align="center">
		<div class="cell vertical-middle">
			<div class="panel">
				<h4 class="blue">Bitphp</h4>
				<p>
					¡Atención! hubo 
					<span class="highlight"><?php echo count($errors) ?></span> 
					errores en la ejecución de aplicación.
					<?php 
						#para verificar qué se hayan registrado los errores
						if(false === $errors[0]['identifier']):
					?>
						No se pudieron registrar los errores verifica qué haya
						permisos de escritura en 
						<span class="highlight">/core/Bitphp/Log/Errors.log</span>
					<?php
						else:
					?>
						Se registraron los errores en la vitacora de errores 
						<span class="highlight">/core/Bitphp/Log/Errors.log</span>
					<?php
						endif;
					?>
				</p>
			</div>
			<?php 
				  $counter = 1;
			      foreach ($errors as $error): ?>
					<div class="panel">
						<p>
							<span class="highlight"><?php echo $counter ?>: </span>
							<span class="highlight"><?php echo $error['identifier'] ?></span>
						</p>
						<p>
							<?php echo $error['message'] ?>.
						</p>
						<p>
							En <span class="highlight"><?php echo $error['file'] ?></span> 
							en la linea <span class="highlight"><?php echo $error['line'] ?></span>
						</p>
						<?php if(false !== $error['identifier']): ?>
									<p class="hide">
										Para consultar detalles:<br><br>
										<span class="comand">
										~(/cli)$ php dummy error <?php echo $error['identifier'] ?>
										</span>
									</p>
						<?php endif; ?>
					</div>
			<?php 
				  $counter++;
				  endforeach; 
			?>
		</div>
	</div>