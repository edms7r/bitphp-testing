<?php namespace Bitphp\Base;

	# para las variables de ambito global
	# qué necesite bitphp
	$_BITPHP = array();

	use \Bitphp\Core\Error;
	use \Bitphp\Core\Config;

	/**
	 *	Base para las aplicaciones de bitphp
	 *	carga las clases base de bitphp
	 */
	abstract class Server {

		/**
		 *	Crea una direccion base del servidor 
		 *	eg. http://foo.com/
		 *	    https://foo.com/test
		 *	Dependiendo de donde se encuentre
		 */
		private function getBaseUri() {
			$base_uri  = empty($_SERVER['HTTPS']) ? 'http://' : 'https://';
			$base_uri .= $_SERVER['SERVER_NAME'];
			$dirname = dirname($_SERVER['PHP_SELF']);
			$base_uri .= $dirname == '/' ? '' : $dirname;
			return $base_uri;
		}

		public function __construct() {
			global $_BITPHP;

			$_BITPHP['BASE_PATH'] = realpath('');
			$_BITPHP['BASE_URI'] = $this->getBaseUri();
			$_BITPHP['REQUEST_URI'] = isset($_GET['_uri']) ? $_GET['_uri'] : '';

			#se define archivo de configuración
			Config::load($_BITPHP['BASE_PATH'] . '/app/config.json');
			$errorHandler = new Error();
		}

		abstract public function run();
	}